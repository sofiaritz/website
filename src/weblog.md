---
layout: simple_page.njk
tags: meta
title: weblog
---

# weblog
Aquí iré hablando sobre ideas, experiencias y demás cosas que podrían ser relevantes!

### 2023
- [Note Taking (II). Retos](/weblog/note-taking-2) Retos encontrados por el camino
- [Note Taking (I). Notas encriptadas](/weblog/note-taking-experiment) Un programa experimental para encriptar notas.
- [Primer post](/weblog/firstpost) El nacimiento de mi weblog! :)

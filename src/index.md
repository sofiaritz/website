---
layout: simple_page.njk
---

# inicio

Es probable que andes aquí por mis [proyectos](/projects), si eso es lo que te
interesa, puedes ir a esa sección directamente.

# sobre mí
Soy Sofía, aunque la gente me llama Sofi.

Llevo un tiempo programando y me encanta lo divertida y abierta que es la web, además de todas las posibilidades que nos
ofrece, a día de hoy se está perdiendo esta esencia de la web y mi objetivo es traerla de vuelta.

Me gusta mucho [Rust](https://es.wikipedia.org/wiki/Rust_(lenguaje_de_programaci%C3%B3n)) y pese a no ser perfecto,
tengo la esperanza de que sirva como precedente en el futuro de la programación. Aunque comencé a aprender
Rust _circa 2020_, llevo un tiempo en este mundillo y me manejo con mucha soltura.

A día de hoy me siento cómoda en Rust, [JavaScript](https://es.wikipedia.org/wiki/JavaScript)
[y derivados](https://es.wikipedia.org/wiki/TypeScript) y
[C](https://es.wikipedia.org/wiki/C_(lenguaje_de_programaci%C3%B3n)) (_aunque aún me queda mucho por aprender_).

Me encantan los [lenguajes construidos](https://es.wikipedia.org/wiki/Lengua_construida) y
tengo especial interés en [Toki Pona](https://es.wikipedia.org/wiki/Toki_pona), ando en proceso de aprenderlo.
Además de esto, me encanta [Wikipedia](https://es.wikipedia.org/wiki/Wikipedia:Portada) y en mis ratos libres intento
aportar mi granito de arena editando algunos artículos.

Me encanta el concepto de la web porque permite que todo el mundo pueda tener su lugar donde poder hablar, comunicarse
entre sí y desarrollar ideas y pensamientos. A día de hoy, esto ha cambiado, y la web no es abierta como era
anteriormente. Por esto estoy completamente a favor de los
[estándares abiertos](https://es.wikipedia.org/wiki/Est%C3%A1ndar_abierto) y de 
la [descentralización](https://es.wikipedia.org/wiki/Descentralizaci%C3%B3n).

---
layout: simple_page.njk
title: contacto
---

# contacto

Todavía no tengo una gran presencia digital, pero la poquita que tengo aquí se encuentra:

### contacto directo

- E-mail: [sofi@sofiaritz.com](mailto:sofi@sofiaritz.com)
- Matrix: [@sofiaritz:matrix.org](https://matrix.to/#/@sofiaritz:matrix.org)

### redes sociales

- Forgejo: [sofia@git.sofiaritz.com](https://git.sofiaritz.com/sofia)
- Codeberg: [sofiaritz@codeberg.org](https://codeberg.org/sofiaritz)
- Fediverse (Mastodon): [@me@sofiaritz.com (@sofiaritz@hachyderm.io)](https://hachyderm.io/@sofiaritz)

### cifrado y firmado

Puedes encontrar mi clave PGP pública en [/keys/pub.asc](/keys/pub.asc), también disponible en
[los keyservers de ubuntu](https://keyserver.ubuntu.com/).

La huella digital del certificado es: <u>4BB1 6A74 5DE1 C776 5CF3 8788 90B5 116E 3542 B28F</u>.

### canary

Puedes encontrar mi canary en [/canary.txt](/canary.txt). Este es un documento actualizado con regularidad donde confirmo mi identidad y estado actual.

Si en algún momento este documento se encuentra desactualizado o desaparece, probablemente haya ocurrido algo malo.

---

<details>
    <summary>El texto de esta sección se encuentra firmado:</summary>
    <pre>
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

contacto

Todavía no tengo una gran presencia digital, pero la poquita que tengo aquí se encuentra:
contacto directo

    E-mail: sofi@sofiaritz.com
    Matrix: @sofiaritz:matrix.org

redes sociales

    Forgejo: sofia@git.sofiaritz.com
    Codeberg: sofiaritz@codeberg.org
    Fediverse (Mastodon): @me@sofiaritz.com (@sofiaritz@hachyderm.io)

cifrado y firmado

Puedes encontrar mi clave PGP pública en /keys/pub.asc, también disponible en los keyservers de ubuntu.

La huella digital del certificado es: 4BB1 6A74 5DE1 C776 5CF3 8788 90B5 116E 3542 B28F.
canary

Puedes encontrar mi canary en /canary.txt. Este es un documento actualizado con regularidad donde confirmo mi identidad y estado actual.

Si en algún momento este documento se encuentra desactualizado o desaparece, probablemente haya ocurrido algo malo.
-----BEGIN PGP SIGNATURE-----

iHUEARYKAB0WIQRLsWp0XeHHdlzzh4iQtRFuNUKyjwUCZA22YwAKCRCQtRFuNUKy
jxPqAP0WLIsgMpnyWORYRbDXMBABIuBIZTm46OSlxY6cS80+oQD+IOaBEcJhwoGx
JkwkXJPGbxrKiMC+14E5Lc/rl6SlyQg=
=qa4I
-----END PGP SIGNATURE-----
</pre>
</details>

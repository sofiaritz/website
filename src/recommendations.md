---
layout: simple_page.njk
title: recomendaciones
---

# recomendaciones
_Aquí iré poniendo recomendaciones sobre juegos, libros, música, etc._

## libros
### Un mundo feliz (Aldous Huxley)

[Un mundo feliz](https://es.wikipedia.org/wiki/Un_mundo_feliz) es un libro, en mi opinión, muy bien escrito y relevante
en el día de hoy por los temas que se tratan.

Este libro es la contraparte [distópica](https://es.wikipedia.org/wiki/Distop%C3%ADa) de
[La isla](https://es.wikipedia.org/wiki/La_isla_(Aldous_Huxley)), otro libro que también puedo recomendar
orgullosamente.

## videojuegos
### OMORI

[OMORI](https://www.omori-game.com/) es un juego de _terror psicológico_
(aunque el 90% del tiempo es un juego muy tranquilo y relajante).

Tiene una historia un tanto dura, y la manera en que se cuenta no podía estar más pulida. No podría recomendarlo más,
no puedo contar mucho (para evitar spoilers), pero se lo recomendaría a cualquier persona que le gusten los juegos de
historia y RPG y compañía.

### a short hike

[a short hike](https://ashorthike.com/) es un juego muy muy tranquilo y relajante. Es un juego para olvidarte del mundo
y relajarte un ratito.

Tiene una historia que pese a poder ser considerada simple, la manera en que se descubre y la compañía que los escenarios y la música dan al descubrimiento es sublime. Un juego muy tranquilo, muy bonito, muy divertido con una historia bien contada. Otro juego 100% recomendado.

### Milo and the Magpies

[Milo and the Magpies](https://store.steampowered.com/app/1407420/Milo_and_the_Magpies/) es un juego de puzles muy
corto, con una historia juguetona pero bastante simple que no decepciona. El arte y música son geniales y pese a poder
completarse en un par de horas, merece la pena por lo relajante y bello que es el juego.

Hay algunos puzles donde te puedes atascar con cierta facilidad, pero aun así, sigue siendo un juego con un concepto
bueno y simple, y con una muy buena ejecución.

---
layout: simple_page.njk
title: proyectos
---

# proyectos

## GFonts

[GFonts](https://git.sofiaritz.com/GFonts) es un proyecto cuyo propósito es ofrecer una alternativa de código abierto y
_[auto-hospedada](https://en.wikipedia.org/wiki/Self-hosting_(web_services))_ a [Google Fonts](https://fonts.google.com/).

### motivación

Tal y como tengo expuesto en [mi _mirror_ de Google Fonts](https://cdn.sofiaritz.com/fonts/), el uso de CDNs como Google
Fonts permite que se pueda rastrear la actividad de los usuarios en la web. Gracias a herramientas como
[Decentraleyes](https://decentraleyes.org/) podemos reducir los efectos de estas CDNs, pero no mitigarlos completamente.

GFonts quiere proveer una manera rápida y sencilla para que todos puedan crear su propia _mini-CDN_ de Google Fonts y
hacer uso de ella en lugar de la provista por Google. Este proyecto
[ya está en funcionamiento](https://git.sofiaritz.com/GFonts/gfonts-interface#usage), y esta misma web lo utiliza :)

---
Además de esto, puedo decir que he creado varios proyectos full-stack, librerías relacionadas con cifrado en Rust y
herramientas enfocadas a DX.

## proyectos irrelevantes
### note taking | [repo](https://git.sofiaritz.com/sofia/note-taking)
Una app creada con [egui](https://github.com/emilk/egui) con el objetivo de ser un sistema experimental para tomar
notas.

Las notas están encriptadas por defecto (excepto los metadatos) y en un futuro añadiré soporte para Markdown básico y demás.

### markdown cv | [repo](https://git.sofiaritz.com/sofia/markdown-cv) [instancia](https://md-cv.sofiaritz.com/)
Una web simple que permite la creación y envío de CVs creados en Markdown con un tema simple y bonito por defecto.

Creada con Svelte y [marked](https://github.com/markedjs/marked). Debo mejorar ciertas partes del código
(obtención de CVs remotos, etc), pero sin duda funciona.

### sofi web | [repo](https://codeberg.org/sofiaritz/website) [mirror](https://git.sofiaritz.com/sofia/website)
Una web simple, con contenido estático hecha utilizando [11ty](https://www.11ty.dev/).

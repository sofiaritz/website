---
layout: simple_page.njk
english: true
---

# overview

This page was originally written in Spanish, and it is only partially translated to English. You can see the original
page [here](/). If you find any typos, [open an issue](https://git.sofiaritz.com/sofia/website/issues) or
[get in touch with me](/en/contact).

# about me

Hi! I'm Sofía.

I've been doing programming for a while, and I love how open and fun is the Internet! This fun and openness is being lost
and my objective is to bring them back.

I love [Rust](https://rust-lang.org), and I hope it helps shape the future of programming. Even though I started to learn Rust <u>circa 2020</u>, I've been
here for a while, and I'm quite proficient.

I feel comfortable in Rust, [JavaScript](https://en.wikipedia.org/wiki/JavaScript),
[TypeScript](https://en.wikipedia.org/wiki/TypeScript), and [C](https://en.wikipedia.org/wiki/C_(programming_language)).

I love [conlangs](https://en.wikipedia.org/wiki/Constructed_language) and I'm learning
[Toki Pona](https://en.wikipedia.org/wiki/Toki_Pona).
I also love [Wikipedia](https://en.wikipedia.org/wiki/Main_Page), and I try to help in my free time.

I love the concept of the Internet because it gives everyone their own place to talk, think and share. Nowadays,
this [has](https://en.wikipedia.org/wiki/Monopoly) [changed](https://en.wikipedia.org/wiki/Echo_chamber_(media)),
and the web isn't has open as it used to be. I fully support
[open standards](https://en.wikipedia.org/wiki/Open_standard), and
[decentralization](https://en.wikipedia.org/wiki/Decentralization#Technological_decentralization).

---
layout: simple_page.njk
title: contact
english: true
---

# contact

### direct contact

- E-mail: [sofi@sofiaritz.com](mailto:sofi@sofiaritz.com)
- Matrix: [@sofiaritz:matrix.org](https://matrix.to/#/@sofiaritz:matrix.org)

### social media

- Forgejo: [sofia@git.sofiaritz.com](https://git.sofiaritz.com/sofia)
- Codeberg: [sofiaritz@codeberg.org](https://codeberg.org/sofiaritz)
- Fediverse (Mastodon): [@me@sofiaritz.com (@sofiaritz@hachyderm.io)](https://hachyderm.io/@sofiaritz)

### encryption and signing

You can find my public PGP key at [/keys/pub.asc](/keys/pub.asc), it's also available at the
[ubuntu keyservers](https://keyserver.ubuntu.com/).

The fingerprint of the cert is: <u>4BB1 6A74 5DE1 C776 5CF3 8788 90B5 116E 3542 B28F</u>.

### canary

You can find my canary at [/canary.txt](/canary.txt). This document is updated and signed frequently.

---

<details>
    <summary>The text of this section is signed</summary>
    <pre>
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

contact
direct contact

    E-mail: sofi@sofiaritz.com
    Matrix: @sofiaritz:matrix.org

social media

    Forgejo: sofia@git.sofiaritz.com
    Codeberg: sofiaritz@codeberg.org
    Fediverse (Mastodon): @me@sofiaritz.com (@sofiaritz@hachyderm.io)

encryption and signing

You can find my public PGP key at /keys/pub.asc, it's also available at the ubuntu keyservers.

The fingerprint of the cert is: 4BB1 6A74 5DE1 C776 5CF3 8788 90B5 116E 3542 B28F.
canary

You can find my canary at /canary.txt. This document is updated and signed frequently.
-----BEGIN PGP SIGNATURE-----

iHUEARYKAB0WIQRLsWp0XeHHdlzzh4iQtRFuNUKyjwUCZDhElwAKCRCQtRFuNUKy
j13iAQC+B3VZgDkSrm4F+wz+GZFgU1SFva8qUAbhG0YrBOjM/QEAxZLtev7DxM2L
xHZiTLuQzFD2kN3m9K4Kh1/osJ38eAg=
=rE2M
-----END PGP SIGNATURE-----
</pre>
</details>

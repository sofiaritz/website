---
layout: post.njk
title: First post
tags:
    - meta
date: 2023-04-22
english: true
---

This is the first post in my English weblog! I'll post translations of my [Spanish weblog](/weblog) and some
English-specific things!

Right now there are some things missing, but I'll improve this weblog over time :)

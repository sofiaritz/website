---
layout: post.njk
title: Primer post
tags:
    - meta
date: 2023-03-12
---

Este es el primer post del weblog! Aquí iré compartiendo ideas, ocurrencias y experiencias a medida que me mueva por
los mundos de Internet y la programación.

Ahora mismo este weblog está parcialmente incompleto, pero dentro de poco añadiré las cosas que faltan para que esté
completamente a punto ([RSS](https://es.wikipedia.org/wiki/RSS), etiquetas, comentarios, etc).

Cualquier sugerencia o idea para este weblog podéis [enviármela](/contact) sin problema!

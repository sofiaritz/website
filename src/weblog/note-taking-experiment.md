---
layout: post.njk
title: Note Taking. Notas encriptadas
tags:
    - rust
    - experimentos
    - note taking
date: 2023-03-12
# comment_status: "109965950074102272"
---

## motivación

Un día me di cuenta de que mi _setup_ para tomar notas era bastante engorroso: abría el bloc de notas, escribía, guardaba
el .txt, abría [Kleopatra](https://www.gpg4win.org/about.html), esperaba a que se cargara la base de datos, etc.

No es difícil darse cuenta de que este sistema no era _el óptimo_. Por tanto, decidí embarcarme en la búsqueda de
algún programa que cumpliera los siguientes requisitos:
1. **Offline**. No quiero mis notas, encriptadas o no, viajando por lugares desconocidos.
2. **Seguro**. No quiero mis notas siendo expuestas por alguna vulnerabilidad o problema de diseño.
3. **Simple**. No quiero tener que dar mil vueltas para escribir un documento de 50 palabras.

Y todo lo que encontraba no cumplía todos los requisitos: o tenía _sincronización en la nube_, o la interfaz era muy
compleja, etc.

Por tanto, decidí que debía arreglar esto yo misma: y así surgió
[note taking](https://git.sofiaritz.com/sofia/note-taking).

## idea

He estado un par de semanas experimentando con un concepto relativamente simple:
_un programa que permita crear y almacenar notas encriptadas_.

Este concepto es sencillo, pero la ejecución es lo importante. Me establecí una serie de objetivos que permitieran
poder tener una base sobre la que trabajar, y estos objetivos eran los siguientes:
- **Simple**. Una aplicación fácil de usar para todo el mundo, con y sin conocimientos.
- **Seguro**. Las notas no deben ser susceptibles de ataques de fuerza bruta y compañía.
- **Completo**. Se debe ofrecer una gran variedad de opciones y posibilidades a la hora de redactar y ver las notas.

## funcionamiento

note taking tiene un funcionamiento bastante sencillo. Cuando abres la aplicación ocurre lo siguiente:
1. Te pide que introduzcas la contraseña utilizada para encriptar la "_base de datos_".
2. Comprueba la contraseña utilizando notas existentes.
3. Si es correcta, carga todas las notas encriptadas en memoria para ser desencriptadas bajo demanda.

Una vez has pasado esta etapa inicial, todo es muy intuitivo. Para añadir una nota haces click en el botón que dice
"_want to add one note?_", para desencriptar una nota haces click en el botón que dice "_decrypt note_", etc.

Además de esto, el apartado de configuración actualmente tiene la opción de exportar las notas en formato JSON para
poder ser importadas por programas compatibles con el formato. En un futuro el apartado de configuración tendrá más
opciones, como importar notas, cambiar la contraseña y demás.

## seguridad

Recientemente, se cambió como se manejaban las contraseñas en note taking. Ahora mismo se sigue el siguiente sistema:
1. El usuario introduce la contraseña
2. La contraseña pasa por una función de derivación
([KDF](https://crypto.stackexchange.com/questions/40757/key-derivation-functions-kdf-what-are-they-what-are-their-main-purposes-and))
   1. Se calcula el SHA256 de la contraseña, que se utilizará como 
   [_salt_](https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html#salting).
   2. Se usa [Argon2](https://en.wikipedia.org/wiki/Argon2) para crear el hash de la contraseña.
   3. Se codifica el resultado en hexadecimal.
3. Se utiliza [pwbox](https://docs.rs/pwbox/0.5.0/pwbox/) con la contraseña derivada.

Este sistema evita que se puedan forzar las contraseñas:
1. Todas las contraseñas tienen gran entropía al salir del KDF.
2. El KDF tiene gran complejidad, lo que ralentiza los ataques de fuerza bruta.

Además de esto, se utilizan ciertos sistemas para _sugerir_ al usuario el uso de contraseñas seguras. Un ejemplo de esto
es cómo se le muestran al usuario tanto la longitud como entropía de la contraseña introducida con un sistema de colores
bastante explícito que _invita_ al usuario a emplear contraseñas seguras.

Todo esto se puede ver en mayor detalle en
[_Security of the encrypted notes_ · sofia@git.sofiaritz.com/note-taking#1](https://git.sofiaritz.com/sofia/note-taking/issues/1).

## planes de futuro

En el futuro me gustaría añadir un sistema "_Markdown_" básico para que se puedan añadir cosas como letra en cursiva,
negrita, imágenes, etc.

Pese a que esto pueda parecer relativamente sencillo, especialmente teniendo en cuenta que
[ya existen librerías](https://crates.io/crates/egui_commonmark) que se encargan de esto, cosas como imágenes pueden
ser un gran vector de ataque para intentar desanonimizar u obtener información del usuario. Por tanto, esta clase de
decisiones deben ser tomadas con mucho estudio y cautela.

Además de esto, me gustaría mejorar la privacidad de ciertos aspectos de las notas (metadatos, etc.) y expandir los
ajustes.

## quiero probarlo!

Ahora mismo [no estoy distribuyendo ejecutables](https://git.sofiaritz.com/sofia/note-taking/issues/2), pero es muy
fácil compilar el proyecto si ya tienes Rust y Cargo.

Si ya tienes Rust y Cargo [instalados](https://www.rust-lang.org/tools/install) y
[actualizados](https://rust-lang.github.io/rustup/basics.html#keeping-rust-up-to-date), ejecuta los siguientes comandos:
1. `git clone https://git.sofiaritz.com/sofia/note-taking.git`
2. `cd note-taking`
3. `cargo run --release`

(Si el último paso falla, puede que tengas que utilizar `cargo +nightly build --release`)

## ideas?

Cualquier idea es bienvenida! [Ponte en contacto conmigo](/contact) o abre una
[issue](https://git.sofiaritz.com/sofia/note-taking/issues) en el repositorio. :)

## imágenes

<img alt="Captura de pantalla donde se muestra una entrada de texto con los valores de entropía y longitud a la derecha en color verde, indicando así valores aceptables." src="/assets/weblog/note-taking-experimental/password-prompt.png" width="85%"/>
<img alt="Captura de pantalla donde se muestran los botones 'want to add one note?' y 'settings'" src="/assets/weblog/note-taking-experimental/top-buttons.png" width="85%"/>
<img alt="Captura de pantalla donde se muestra la lista de notas. En cada nota se muestra el título y su derecha la fecha. Bajo el título hay un botón que indica 'decrypt note'" src="/assets/weblog/note-taking-experimental/note-list.png" width="85%"/>
<img alt="Captura de pantalla donde se muestra el formulario de creación de notas, con diversas entradas de texto para el título, texto y metadatos. Al final hay un botón con el texto 'add note'" src="/assets/weblog/note-taking-experimental/note-creation.png" width="85%"/>


const { DateTime } = require("luxon")
const timeToRead = require("eleventy-plugin-time-to-read")

module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy({ "static": "/" })

    eleventyConfig.addPlugin(timeToRead, {
        language: "es",
        style: "long",
        hours: "auto",
        minutes: true,
    })

    eleventyConfig.addFilter("readableDate", dateObj => {
        return DateTime.fromJSDate(dateObj, {zone: 'utc'}).setLocale("es-ES").toFormat("dd LLL yyyy");
    })

    // https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-date-string
    eleventyConfig.addFilter('htmlDateString', dateObj => {
        return DateTime.fromJSDate(dateObj, {zone: 'utc'}).toFormat('yyyy-LL-dd');
    })
    return {
        passthroughFileCopy: true,
        dir: {
            input: "src"
        }
    }
}

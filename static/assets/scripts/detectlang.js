const SPANISH_MESSAGE = "Versión en español"
const SPANISH_BUTTON = "No mostrar esto de nuevo"
const ENGLISH_MESSAGE = "English page"
const ENGLISH_BUTTON = "Do not show me this again"

if (redirect_notice_enabled()) {
    let language = (navigator.language || navigator.userLanguage).split("-")[0]
    if (location.pathname.startsWith("/en")) {
        if (language === "es") {
            add_redirect_notice(location.pathname.replace("/en", "").slice(0, -1), SPANISH_MESSAGE, SPANISH_BUTTON)
        }
    } else if (!["recommendations", "projects", "links"].includes(location.pathname.split("/")[1])) {
        if (language !== "es") {
            add_redirect_notice("/en" + location.pathname.slice(0, -1), ENGLISH_MESSAGE, ENGLISH_BUTTON)
        }
    }
}

function add_redirect_notice(redirect, message, btn_message) {
    let main = document.getElementsByTagName("main").item(0)
    let div = document.createElement("div")
    div.classList.add("redirect-notice")
    div.innerHTML = `<div><span>${message}:</span> <a href="${redirect}">${redirect}</a></div>`

    let button = document.createElement("button")
    button.addEventListener("click", () => {
        disable_redirect_notice()
        location.reload()
    })
    button.innerText = btn_message
    div.append(button)

    main.prepend(div)
}

function redirect_notice_enabled() {
    return localStorage.getItem("i18n:no_redirect_notice") !== "1"
}

function disable_redirect_notice() {
    localStorage.setItem("i18n:no_redirect_notice", "1")
}
